const getEthPrice = () => {
    return content.fetch('https://min-api.cryptocompare.com/data/price?fsym=ETH&tsyms=EUR&extraParams=cryptokittyconvert')
        .then(res => res.json());
}

const getNotes = (prices) => {
    const price = prices[Object.keys(prices)[0]];
    const notes = Array.from(document.querySelectorAll('.KittyStatus-note'));
    notes.map(note => {
        note.innerHTML+=` | ${(parseFloat(note.childNodes[2].nodeValue) * price).toFixed(2)}€`;
    });
}

const waitForKitties = () => {
    const grid = document.querySelector('.KittyStatus-item');
    if (grid) {
        clearInterval(iv);
        getEthPrice()
            .then(getNotes);
        iv = null;
        if (!observes) {
            observes = true;
            observer.observe(document.querySelector('.Main'), {childList:true});
        }
    }
}

let observes = null;

let iv = setInterval(waitForKitties, 100);

var observer = new MutationObserver(function(mutations) {
    mutations.forEach(function(mutation) {
        
        if (iv) {
            clearInterval(iv);
        }

        iv = setInterval(waitForKitties, 100);
    });    
  });